#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
Write a program that uses nested loops to collect data and calculate the
average rainfall over a period of years. The program should first ask for
the number of years. The outer loop will iterate once for each year.
The inner loop will iterate twelve times, once for each month.
Each iteration of the inner loop will ask the user for the inches of rainfall for that month.
After all iterations, the program should display the number of months,
the total inches of rainfall, and the average rainfall per month for the entire period.
"""


def integer_input(user_query=""):
    """
    Safely handles user input into an int obj that the program can use.
    Failing that it recalls on the user to try another value or exit the program.
    :param user_query: Given string to output to the user/
    :return: An integer representation of the input string.
    """
    user_input = input(user_query)

    """ User could just Ctrl^D but added a obvious breakout"""
    if user_input.upper().strip() is "QUIT":
        print(" \n\r")
        exit(0)

    if user_input.isdigit():
        try:
            user_input = int(user_input)
        except ValueError:
            raise ValueError("User input of {} could no be safely converted for calculations.", user_input)
    else:
        print("Unable to extract a int value from '{0}' \nPlease try again... "
              "(or Quit by typing 'quit')\n".format(user_input))
        integer_input(user_query)
    return user_input


def month_rainfall_query_generator():
    """
    Generator function to query along the month of a year offering this output to the user via #integer_input
    :return: An integer care of the #integer_input function.
    """
    index = 0
    months = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December", ]

    while index < len(months):
        yield integer_input("Enter the inches of rainfall for " + months[index] + " :")
        index += 1


def calculate(years=1):
    """
    Iterates over the given n of years recording and calculating data sets of;
        year number (zero indexed),
        year rainfall total,
        year rainfall average based on total.

    :param years: integer obj of years to loop over.
    :return: A 2D list of [year zero index'][year data]
    """
    list_of_years = []

    for year in range(years):
        print("For year [{}]:".format(year + 1))

        month_rainfall = month_rainfall_query_generator()  # Generator object
        year_totals = 0
        list_of_years.append([])

        for month in range(12):
            year_totals += next(month_rainfall)         # Gets month rainfall from user input.

        list_of_years[year].append(year)                # Add the year count.
        list_of_years[year].append(year_totals)         # Add the year total record.
        list_of_years[year].append(year_totals / 12)    # Add the year average calculation.

        print("\n")  # Output legibility helper.

    return list_of_years


def print_results(data):
    """
    Prints the results of the given 2d list data
    :param data: A 2D list where [n][0] is the year, [n][1] is the year total, [n][2] is the year average.
    :return:
    """
    accumulative_totals = 0
    for year in data:
        print("\n-------------YEAR {}-----------------".format(year[0]))
        print("    Total rainfall:  {}".format(year[1]))
        print("    Average rainfall:  {}".format(year[2]))
        print("--------------------------------------")
        accumulative_totals += year[1]

    years = len(data)
    print("\n=================Total==================")
    print("  Total rainfall over {0} years:  {1}".format(years, accumulative_totals))
    print("  Average over {0} years:  {1}".format(years, accumulative_totals / (years * 12)))


if __name__ == "__main__":    # main
    print("### Average rainfall/cycle Calculator ###")
    print_results(calculate(integer_input("please enter the number of years to calculate: ")))

