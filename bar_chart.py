#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
Write a program that asks the user to enter today’s
sales for five stores. The program should display a bar
chart comparing each store’s sales. Create each bar in the
chart by displaying a row of asterisks. Each asterisk should
represent $100 of sales.

"""

# TODO: Add bar scalability.

NUMBER_OF_STORES = 5
SALES_METRIC = 100    # Dollars.
CHART_SYMBOL = '*'


def parse_input_as_int(user_input=""):
    complete = False
    value = ""

    while not complete:
        value = input(user_input)
        """ User could just Ctrl^D but added a obvious breakout"""
        if value.upper().strip() == "QUIT":
            print("\n\r")
            exit(0)

        if value.isdigit():
            try:
                value = int(value)
                complete = True    # Success.
            except ValueError:
                raise ValueError("User input of {} could no be safely converted for calculations.", user_input)
        else:
            print("Unable to extract an int value from '{0}' \nPlease try again... "
                  "(or Quit by typing 'quit')\n".format(user_input))

    return value


def display_comparison_chart(data):

    print("## Store Daily Sales Comparator ##")

    for total in enumerate(data):
        symbol_list = [CHART_SYMBOL for milestones in range(int(total[1] / SALES_METRIC))]
        store_results = "[Store: {0}] $[{1}|  ${2}".format(total[0] + 1, str.join("", symbol_list), total[1])
        print(store_results)

    print("####")


def record_daily_ringup():
    prompt_message = "Sales total for store number {0}: "
    return [parse_input_as_int(prompt_message.format(sales + 1)) for sales in range(NUMBER_OF_STORES)]


if __name__ == "__main__":
    display_comparison_chart(record_daily_ringup())
