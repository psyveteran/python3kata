#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
Serendipity Booksellers has a book club that awards points to its customers based on the number of books purchased each
month. The points are awarded as follows:

If a customer purchases 0 books, he or she earns 0 points.
If a customer purchases 1 book, he or she earns 5 points.
If a customer purchases 2 books, he or she earns 15 points.
If a customer purchases 3 books, he or she earns 30 points.
If a customer purchases 4 or more books, he or she earns 60 points.
Write a program that asks the user to enter the number of books that he or she has purchased this month and displays
the number of points awarded.
"""

reward_table = {
    '0': 0,
    '1': 5,
    '2': 15,
    '3': 30,
    '4': 60, }


def check_points(user_input="0"):
    """
    Simple mapping function that matches a given string input to a rewards table with a top limit.
    :param user_input: User keyboard input.
    :return: Integer value of assigned points.
    """
    global points
    number_key = user_input.strip()

    if number_key and number_key in reward_table:    # String not empty and matching hash.
        points = reward_table.get(number_key)
    elif not number_key:                             # String is empty.
        points = 0
    else:                                            # String is beyond reward table.
        check_points('4')

    return points


if __name__ == "__main__":
    print("#=  Serendipity Booksellers  =#\n\r")
    print("Please enter the number of books purchased.")
    print("The customer is rewarded {0} points".format(check_points(input(":"))))
