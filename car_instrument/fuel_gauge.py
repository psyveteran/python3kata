#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
To know the car’s current amount of fuel, in gallons.
To report the car’s current amount of fuel, in gallons.
To be able to increment the amount of fuel by 1 gallon. This simulates putting fuel in the car. ( The car can hold a maximum of 15 gallons.)
To be able to decrement the amount of fuel by 1 gallon, if the amount of fuel is greater than 0 gallons. This simulates burning fuel as the car runs.
"""


class FuelGauge:

    MAX_GALLONS = 15

    def __init__(self, fuel=15):
        self.fuel = fuel

    def tank_level(self, gallon=1):
        self.fuel += gallon

        """ sorted(MIN, CURRENT, MAX)[1] is equivalent to a clamp(CURRENT, MIN, MAX) """
        self.fuel = sorted((0, self.fuel, self.MAX_GALLONS))[1]

    def full(self):
        return self.fuel == self.MAX_GALLONS








