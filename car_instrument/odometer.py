#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
- To know the car’s current mileage.
- To report the car’s current mileage.
- To be able to increment the current mileage by 1 mile. The maximum mileage the odometer can store is 999,999 miles.
When this amount is exceeded, the odometer resets the current mileage to 0.
- To be able to work with a FuelGauge object. It should decrease the FuelGauge object’s current amount of fuel by
1 gallon for every 24 miles traveled. (The car’s fuel economy is 24 miles per gallon.)
"""


class Odometer:

    def __init__(self, mileage=0):
        self.mileage = abs(mileage)

    def increment(self):
        self.mileage += 1

