#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#
from math import pi


class Circle(object):

    def __init__(self, radius=0.5):
        self.name = self.__class__.__name__
        self.radius = abs(radius)
        self.width = self.radius * 2.0
        self.height = self.radius * 2.0
        self.pi = pi

    def set_radius(self, radius):
        self.radius = abs(radius)

    def get_radius(self, ):
        return self.radius

    def get_area(self, ):
        return pi * pow(self.radius, 2)

    def get_diameter(self, ):
        return self.width

    def get_circumference(self, ):
        return 2.0 * (pi * self.radius)

    def __str__(self) -> str:
        return "{{Name={name}}} " \
               "{{Radius={radius}}} " \
               "{{Diameter=({{Width={width}}} " \
               "{{Height={height})}}"\
            .format(radius=self.radius, name=self.name, width=self.width, height=self.height)





