#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0

"""
Write a Circle class that has the following fields:

radius: a double
PI: a final double initialized with the value 3.14159
The class should have the following methods:

Constructor. accepts the radius of the circle as an argument.
Constructor. A no-arg constructor that sets the radius field to 0.0.
setRadius. A mutator method for the radius field.
getRadius. An accessor method for the radius field.
getArea. Returns the area of the circle which is calculated as * area=PIradiusradius
getDiameter. Returns the diameter of the circle which is calculated as diameter=radius*2
getCircumference. Returns the circumference of the circle, which is calculated as circumference= 2PIradius

Write a program that demonstrates the Circle class by asking the user for the circle's radius, creating a
Circle object, and then reporting the circle's area, diameter, and circumference.
"""

from circle_kata.Circle import Circle


def to_integer(type_in):
    """
    Converts a give user string to an integer representation.
    :param type_in: String supplied by the user or system.
    :return: An integer representation of this string or a fallback to a unit (1).
    """
    try:
        return int(float(type_in))
    except (TypeError, ValueError):
        # Logging as falling back to unit circle
        return 1


def runner():

    print("Circle kata:\n\nInfo() method on a unit circle:")
    unit_circle = Circle()
    print(unit_circle)
    print("Area of this unit circle is: {:.4f}".format(unit_circle.get_area()))
    print("Diameter of this unit circle is: {:.4f}".format(unit_circle.get_diameter()))
    print("Circumference of this unit circle is: {:.4f}\n".format(unit_circle.get_circumference()))

    print("Please enter custom circle size:")
    custom_circle = Circle(to_integer(input()))
    print(custom_circle)
    print("Area of this unit circle is: {:.4f}".format(custom_circle.get_area()))
    print("Diameter of this unit circle is: {:.4f}".format(custom_circle.get_diameter()))
    print("Circumference of this unit circle is: {:.4f}\n".format(custom_circle.get_circumference()))


if __name__ == "__main__":
    runner()
