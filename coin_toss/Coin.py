#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

import random


class Coin:
    """
    Class representation of a 2-sided flippable coin.
    One side is called "heads", the other, "tails".
    """
    from enum import Enum

    class Side(Enum):
        heads = 1
        tails = 2

    def __init__(self, ):
        self.side = next(self.flip())

    def flip(self, ):
        yield self.Side(random.randint(1, 2))

    def toss(self, ):
        self.side = next(self.flip())

    def get_side_up(self):
        return self.side.name
