#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
Write a class named Coin. The Coin class should have the following field:

A String named sideUp. The sideUp field will hold either “heads” or “tails”
indicating the side of the coin that is facing up.
The Coin class should have the following methods:

A no-arg constructor that randomly determines the side of the coin that is facing up
 (“heads” or “tails”) and initializes the sideUp field accordingly.
A void method named toss that simulates the tossing of the coin. When the toss method
is called, it randomly determines the side of the coin that is facing up
(“heads” or “tails”) and sets the sideUp field accordingly.
A method named getSideUp that returns the value of the sideUp field.
Write a program that demonstrates the Coin class. The program should create
an instance of the class and display the side that is initially facing up.
Then, use a loop to toss the coin 20 times. Each time the coin it tossed,
display the side that is facing up. The program should keep count of the number
of times heads is facing up and the number of times tails is facing up, and display
those values after the loop finishes.
"""

from coin_toss.Coin import Coin


def demo(tosses=20):
    """
    Runs a simple demo of an instance of the Coin class.
    Prints out the results and tally of n tosses given.
    :param: tosses, n number of tosses to tally.
    :return: None.
    """
    coin = Coin()

    print("Coin toss \"Coin\" class demo\n")

    outcome = []
    for toss in range(tosses):
        coin.toss()
        outcome.append(coin.get_side_up())
        print("Toss #{toss} resulted in a: {outcome}".format(toss=toss, outcome=outcome[-1]))


    print("Toss spread: 'Heads' = {heads}  'Tails' = {tails}\n".format
          (heads=outcome.count("heads"), tails=outcome.count("tails")))


if __name__ == "__main__":
    demo()
