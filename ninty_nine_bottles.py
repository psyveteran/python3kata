"""
Write a command line program that will print out the lyrics to 99 Bottles.
Bottles of what? Make a variable for what is in the bottle (so it can be easily changed)
and start it out as “beer” or another beverage of your choice, if you would like.
Also, extract a variable to represent the initial number of bottles.
"""

"""
99 bottles of beer on the wall.
99 bottles of beer!
Take one down. Pass it around.
98 bottles of beer on the wall.

98 bottles of beer on the wall. 98 bottles of beer!
Take one down. Pass it around.
97 bottles of beer on the wall.

... and so on until ...

2 bottles of beer on the wall.
2 bottles of beer!
Take one down. Pass it around.
1 bottle of beer on the wall.

1 bottle of beer on the wall.
1 bottle of beer!
Take one down. Pass it around.
No more bottles of beer on the wall.
"""


def bottles(beer=99):
    """
    print out the lyrics to 99 Bottles in english.
    :param beer: positive number of beers.
    :return: None
    """
    while beer > 0:
        print("{0} bottles of beer on the wall.".format(beer))
        print("{0} bottles of beer!".format(beer))
        print("Take one down. Pass it around.")

        beer -= 1
        if beer is 0:
            print("No more bottles of beer on the wall")
        else:
            print("{0} bottles of beer on the wall.\n\r".format(beer))


if __name__ == "__main__":
    bottles()
