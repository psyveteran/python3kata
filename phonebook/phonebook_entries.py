#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
Write a class named PhoneBookEntry that has fields for a person’s name and phone number.
The class should have a constructor and appropriate accessor and mutator methods.
Then write a program that creates at least five PhoneBookEntry objects and stores
them in an ArrayList. Use a loop to display the contents of each object in the ArrayList.
"""
from collections import OrderedDict


class PhoneBook:
    """
    PhoneBook contains functionality to modify and read the contents of a phone book like/lite object.
    A phone book object contains none or more entries and handles duplicate entries where needed.
    """
    class _Entry:
        """
        A basic Entry structure.
        """
        def __init__(self, name, number, ):
            """
            :param name: Split, where possible to fist_name and last_name.
            :param number: string representation of a given number.
            """
            name = str.split(name, " ", 1) if " " in name else name
            self.first_name = name[0] if isinstance(name, list) else name
            self.last_name = name[1] if isinstance(name, list) else "n\\a"
            self.phone_number = [number]

        def __str__(self):
            return "First name: {}\nLast name: {}\nPhone number: {}\n"\
                .format(self.first_name, self.last_name, self.phone_number)

    def __init__(self):
        self._book = OrderedDict()

    def create_contact(self, name, number, __level=1):
        """
        Creates entries in the phone books dictionary.
        Entries are unique and colliding name (keys) are added only if the number is different.
        :param name:
        :param number:
        :param __level:
        :return:
        """
        name = str.strip(name)
        number = str.strip(number)

        new_contact = self._Entry(name, number)

        if name not in self._book:
            self._book.update({name: new_contact})
        elif self._book.get(name).phone_number[0] != number:
            name = str.split(name, " (")[0]
            self.create_contact(name + " ({})".format(__level), number, __level + 1)
        else:
            print("Contact & matching number is already in the Phone book.")
            print(self._book.get(name))

    def __str__(self):
        return str(self._book)

    def list_phone_book(self):
        return sorted(self._book.values(), key=lambda entry: entry.first_name)


def demo():
    book0 = PhoneBook()
    book0.create_contact("John Smith", "0400000000")
    book0.create_contact("John Smith", "0410000000")     # Collision, add as different.
    book0.create_contact("Jordan Smith", "0410000000")
    book0.create_contact("Amy Smith", "0400000000")
    book0.create_contact("Jane Doe", "0400000000")
    book0.create_contact("Jane Doe", "0400000000")       # Duplicate, warn and ignore.
    book0.create_contact("Barry White", "0400000000")
    book0.create_contact("Clair Doe", "0400000000")
    book0.create_contact("Clair Doe", "0410000000")      # Collision, add as different.
    book0.create_contact("Edward Smith", "0410000000")
    book0.create_contact("Edward", "0410000000")
    book0.create_contact("Felicity Smith", "0410000000")
    book0.create_contact("Garry Doe", "0400000000")
    book0.create_contact("Garry Doe", "0400000001")      # garry doe (1)
    book0.create_contact("Garry Doe", "0400000002")      # garry doe (2)
    book0.create_contact("Garry Doe", "0400000003")      # garry doe (3)
    print("\n\r")

    print("--- Address book list: ---")
    for entry in book0.list_phone_book():
        print("------------Entry-----------------")
        print(entry)
    print("----------------------------------")


if __name__ == "__main__":
    demo()
