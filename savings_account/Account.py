#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#


class Account:
    from enum import Enum

    __interest = (0.041, )

    class AccountType(Enum):
        SAVINGS = 1
        TRANSACTION = 2
        CREDIT_CARD = 3
        HOME_LOAN = 4

    class _Saving:

        def __init__(self, interest_rate, default, overdraw_limit, open_status, ):
            self.overdraw_limit = -abs(overdraw_limit)
            self.open_status = open_status
            self.balance = default
            self.interest = interest_rate

        def __str__(self, interest=None):
            interest_info = lambda test: "" if test is None else "      | " + str(self._interest())
            return \
                "Status:: {0} \n      |> Balance:: {si}{1:,.2f} \n" \
                "      | Overdraw limit:: {si}{2:,.2f}\n{interest}" \
                .format(self.open_status, self.balance, self.overdraw_limit, si="$", interest=interest_info(interest))

    def __init__(self, account_type, name, initial_balance=0):
        self.__accounts = {}
        if account_type == self.AccountType.SAVINGS:
            self.__accounts.update({(account_type, name):
                                        self._Saving(self.__interest[0], initial_balance, -200.00, True)})

    def deposit(self, value=0.00, account=AccountType.SAVINGS, name=""):
        if value and value > 0.00 and (account, name.strip()) in self.__accounts:
            self.__accounts.get((account, name.strip())).balance += abs(value)

    def withdraw(self, value=0.00, account=AccountType.SAVINGS, name=""):
        if value and value < 0.00 and (account, name.strip()) in self.__accounts:
            current_acc = self.__accounts.get((account, name.strip()))
            if current_acc.balance - abs(value) > current_acc.overdraw_limit:
                current_acc.balance -= abs(value)

    def get_account(self, by_name, with_account_type):
        if (with_account_type, by_name) in self.__accounts:
            return self.__accounts.get((with_account_type, by_name))

    @staticmethod
    def apply_interest(account=get_account, times=1):
        """
        Calculates and applies the interest added to the account each n times.
        :param account: The account to apply the interest to (compounding)
        :param times: the times to calculate + compound.
        :return:
        """
        if account:
            temp_interest = []
            while times > 0:
                times -= 1
                temp_interest.append(account.balance * account.interest)
                account.balance += temp_interest[-1]
            return ["Month " + str(month) + ": $" + str(round(i, 2)) for month, i in enumerate(temp_interest, start=1)]

    def __str__(self):
        report = "[[ " + self.__class__.__name__ + " ]]:\n"
        for account in self.__accounts.items():
            report += "    " + account[0][0].name + " '" + account[0][1] + "'\n"
            report += "      | " + str(account[1])
        return report
