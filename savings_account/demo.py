#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
Use notepad or another text editor to create a text file named deposits.txt.
The file should contain the following numbers, one per line:

100.00
124.00
78.92
37.55
Next, create a text file named withdrawals.txt. The file should contain the following numbers, one per line:

29.88
110.00
27.52
50.00
12.90
The numbers in the deposits.txt file are the amounts of deposits that were made to a savings account
during the month, and the numbers in the withdrawals.txt file are the amounts of withdrawals that were
made during the month. Write a program that creates an instance of the SavingsAccount class that you
wrote in the programming challenge 10. The starting balance for the object is 500.00.
-->
The program should read the values from the Deposits.txt file and use the object's
method to add them to the account balance. The program should read the values from the
Withdrawals.txt file and use the object's method to subtract them from the account balance.
The program should call the class method to calculate the monthly interest, and then display
the ending balance and the total interest earned.

Calculate the totals of withdrawals and deposits from a file.
If you cannot use java 8, you can read all the lines of the file and then call the deposit or
withdrawal for each line item.
"""

from savings_account.Account import Account as Acc
from os import getcwd


def demo():
    demo_account = Acc(Acc.AccountType.SAVINGS, "super saver", 500.00)

    print("Account demo: \"Monthly deposits +\\- withdrawals\"\n")
    print("Starting account details:")
    print(demo_account)
    print("\n\r")

    def _strip(value):
        value = value.strip()
        return None if str(value).startswith('#') and value else value

    try:
        with open("deposits.txt") as monthly_deposits:
            for deposit in monthly_deposits:
                deposit = _strip(deposit)
                if deposit is not None:
                    demo_account.deposit(to_float(deposit), Acc.AccountType.SAVINGS, "super saver")
    except (FileNotFoundError, FileExistsError) as err:  # TODO: would handle more beyond demo.
        print(str(err) + " \nwas found at: " + getcwd())

    try:
        with open("withdrawals.txt") as monthly_withdrawals:
            for withdraw in monthly_withdrawals:
                withdraw = _strip(withdraw)
                if withdraw is not None:
                    demo_account.withdraw(to_float(withdraw), Acc.AccountType.SAVINGS, "super saver")
    except (FileNotFoundError, FileExistsError) as err:  # TODO: would handle more beyond demo.
        print(str(err) + " \nwas found at: " + getcwd())

    print("End of month:")
    acc1 = demo_account.get_account("super saver", Acc.AccountType.SAVINGS)
    print(demo_account)
    interest_history = demo_account.apply_interest(acc1, times=1)  # 'times' how many months to calc comp interest for.
    print("Interest history: " + str(interest_history))


def to_float(number):
    number = number.strip()
    try:
        return float(number) if number else 0
    except ValueError as err:
        raise err


if __name__ == "__main__":
    demo()
