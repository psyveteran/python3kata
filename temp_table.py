#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
The formula for converting celsius temperature to fahrenheit
 is °C x 9/5 + 32 = °F. Write a program that displays temps up to 100 degrees fahrenheit.
 """

from enum import Enum
TempSystem = Enum('TempSystem', 'CELSIUS FAHRENHEIT')


def temperature(temp=0, system=TempSystem.FAHRENHEIT):
    """
    Calculates the temperature conversion in the given direction, from given to other.
    :param temp: Integer or float value.
    :param system:  Of Enum type:TempSystem
    :return: Float of calculated conversion.
    """
    if system is TempSystem.CELSIUS:
        return (temp * (9.0 / 5.0)) + 32.0
    elif system is TempSystem.FAHRENHEIT:
        return (temp - 32.0) * (5.0 / 9.0)
    else:
        raise AttributeError("'{}' is not a valid temperature.", temp)


def print_temperature_table(from_temp=-100, up_to=100, stepping=1):
    """
    Prints to stdout, lines of equivalent values in temperature.
    :param from_temp: Starting value.
    :param up_to:  Ending value
    :param stepping: increments to move along the stepping by.
    :return: ~ prints out the results.
    :note: TODO: Hardcoded TempSystem for demo.
    """
    for value in range(from_temp, up_to, stepping):
        print("{:.2f}".format(value) + "°C   ==>   " +
              "{:.2f}".format(temperature(value, TempSystem.CELSIUS)) + "°F")


if __name__ == "__main__":
    print_temperature_table(-200, 200)
    print()
