#!/usr/bin/python3 -R


"""
getTotal - This method should accept a two-dimensional array as its argument and return the total of all the values in the array.
getAverage - This method should accept a two-dimensional array as its argument and return the average of all the values in the array.
getRowTotal. - This method should accept a two-dimensional array as its first argument and an integer as its second argument. The second argument should be the subscript of a row in the array. The method should return the total of the values in the specified row.
getColumnTotal - This method should accept a two-dimensional array as its First argument and an integer as its second argument. The second argument should be the subscript of a column in the array. The method should return the total of the values in the specified column.
getHighestInRow - This method should accept a two-dimensional array as its First argument and an integer as its second argument. The second argument should be the subscript of a row in the array. The method should return the Highest values in the specified row in the array.
getLowestInRow - This method should accept a two-dimensional array as its First argument and an integer as its second argument. The second argument should be the subscript of a row in the array. The method should return the lowest values in the specified row in the array.

"""


#
#  :lamjy016@mymail.unisa.edu.au: (cc by-nc-sa) 2019. 4.0
#

def get_total_values(nested_list):
    """
    Calculates the total of all the values in the array.
    :param nested_list: A 1 n'th deep nested list
    :return: combined total of values in the list array.
    """
    print("hello world")


if __name__ == "__main__":
    get_total_values([[1, 2, 3]])




