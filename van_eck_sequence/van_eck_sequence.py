#!/usr/bin/python3 -R
#  :lamjy@pm.me: (cc by-nc-sa) 2019. 4.0
#

"""
-Kata Fragment-
http://oeis.org/A181391

Jan Ritsema Van Eck's submitted sequence.
- The first term is 0 by definition.
- Since we haven’t seen 0 before, the second term is 0.
- Since we have seen a 0 before, one step back, the third term is 1
- Since we haven’t seen a 1 before, the fourth term is 0
- Since we have seen a 0 before, two steps back, the fifth term is 2.
->etc.
"""


from collections import deque
import matplotlib.pyplot as plt


def sequence(loop):
    ve_set = set()
    output = deque([0, ], maxlen=loop + 1)

    while loop > 0:

        if output[-1] in ve_set:
            # Step (count) backwards until you see it again, then append.
            for count, step_val in enumerate(range(-1, -len(output), -1), 1):
                if output[step_val - 1] is output[-1]:
                    # Append the count's value to the sequence.
                    output.append(count)
                    break
        else:
            ve_set.add(output[-1])    # Add it to the seen.
            output.append(0)          # Append a zero.

        loop -= 1
    return output


if __name__ == "__main__":

    result = sequence(1000)
    line, = plt.plot(result, '-')
    plt.show()
    print("Pattern= {}".format(result))

    # sequence(1000)    # A181391


